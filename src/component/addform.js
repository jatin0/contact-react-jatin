import React, { Component } from 'react';

class addForm extends Component {
    render() {
        return ( 
            <div className="" id="addModal" role="dialog" aria-labelledby="add" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">New message</h5>
                            <button onClick={this.props.onCloseAddForm} type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit = {this.props.addValue}>
                            <div className="form-group">
                                <input type="text" className="form-control" id="first_name" placeholder="First Name"></input>
                            </div>
                            <div className="form-group">
                                <input type="text" className="form-control" id="last_name" placeholder="Last Name"></input>
                            </div>
                            <div className="form-group">
                                <input type="text" className="form-control" id="email" placeholder="email" ></input>
                            </div>
                            <div className="form-group">
                                <input type="tel" className="form-control" id="phone" placeholder="phone no."></input>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary">submit</button>
                            </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button onClick={this.props.onCloseAddForm} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            {/* <button type="button" className="btn btn-primary">Send message</button> */}
                        </div>
                    </div>
                </div>
            </div>  
        )
    }
}

export default addForm;
