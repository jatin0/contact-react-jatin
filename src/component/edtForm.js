import React, { Component } from 'react';

class EditContact extends Component {

    render() { 
        // console.log(this.props.contactDetails);
        return (   
            <div className="" id="editbutton" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">New message</h5>
                            <button onClick={this.props.onClose} type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div className="modal-body">
                        <form id={this.props.contactDetails.id} onSubmit={this.props.ContactEdit}>
                            <div className="form-group">
                                <label className="col-form-label">First Name</label>
                                <input type="text" className="form-control" id="first_name" defaultValue={this.props.contactDetails.first_name} ></input>
                            </div>
                            <div className="form-group">
                                <label className="col-form-label">Last Name</label>
                                <input type="text" className="form-control" id="last_name" defaultValue={this.props.contactDetails.last_name}></input>
                            </div>
                            <div className="form-group">
                                <label className="col-form-label">Email</label>
                                <input type="text" className="form-control" id="email" defaultValue={this.props.contactDetails.email}></input>
                            </div>
                            <div className="form-group">
                                <label className="col-form-label">Phone no. </label>
                                <input type="tel" className="form-control" id="phone" defaultValue={this.props.contactDetails.phone}></input>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-warning" data-dismiss="modal">submit</button>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button onClick={this.props.onClose} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        {/* <button type="button" className="btn btn-primary">Send message</button> */}
                    </div>
                </div>
            </div>
        </div>
        );
    }
}
 
export default EditContact;