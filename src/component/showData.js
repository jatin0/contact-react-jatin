import React, { Component } from 'react';
import JsonData from '../data/contacts.json'
import AddHandler from './addform'
import EditContact from './edtForm'

localStorage.setItem('contacts', JSON.stringify(JsonData));

class ShowData extends Component {
    constructor(props){
        super(props);
        const newContacts = JSON.parse(localStorage.getItem('newContact')) 
        // console.log(typeof newContacts)
        if(newContacts.length !== 0)
        {
            this.state = {  
                add : false,
                edit : false,
                NewData : newContacts,
                editContact: {},
                SearchInput: ""
            } 
        }
        else{
            this.state = {  
                add : false,
                edit : false,
                NewData : JsonData,
                editContact: {},
                SearchInput: ""
            }
        }
        
    }

    componentDidUpdate = () => {
        localStorage.setItem('newContact', JSON.stringify(this.state.NewData));
    }

    deleteHandler = (val) => {
        const deleteData = this.state.NewData.filter(Element => Element.id !== val)
        this.setState({
            NewData : deleteData,
        })
    }
    
    AddContact = (event) => {
        event.preventDefault();
        const contact = {
			avatar_url: 'https://robohash.org/etquiaquia.png?size=100x100&set=set1',
			id: this.state.NewData.length + 1,
			first_name: event.target.first_name.value,
			last_name: event.target.last_name.value,
			email: event.target.email.value,
			phone: event.target.phone.value	
		};
		let newContact = [ ...this.state.NewData ];
		newContact.unshift(contact);
		this.setState({
            NewData: newContact,
            add : !this.state.add,
		});
    }

    addFormWhenClick = () => {
        this.setState({
            add : !this.state.add,
        })
    }

    editFormWhenClick = (details) => {
        this.setState({
            edit : !this.state.edit,
            editContact : details,
        })
    }

    SearchHandler = (event) => {
		this.setState({
            SearchInput: event.target.value,
		});
    }

    closeModal = () => {
        this.setState({
            edit : !this.state.edit,
        })
        // console.log('inside closeModal')
    }

    closeModalForAddBtn = () => {
        this.setState({
            add : !this.state.add,
        })
        // console.log('inside closeModal')
    }
    
    editContactHandler = (event) => {
        event.preventDefault();
        let Contact = [ ...this.state.NewData ];
        const editDetails = Contact.map(val => {
            if(val.id === parseInt(event.target.id))
            {
                val.first_name = event.target.first_name.value
			    val.last_name = event.target.last_name.value
			    val.email = event.target.email.value
                val.phone = event.target.phone.value
            }
            return val;
        })
		this.setState({
            NewData: editDetails,
            edit : !this.state.edit,
        });
    }
    
    render() {
        return (  
            <div>
                 <h4>contacts Details</h4>
                <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1"></span>
                        </div>
                    <input onChange={this.SearchHandler} type="text" className="form-control " placeholder="Username" aria-label="Username" aria-describedby="basic-addon1"></input>
                    <button onClick={this.addFormWhenClick} type="button" className="btn btn-secondary " data-toggle="modal" data-target="#addModal" >Add</button>
                </div>
               
               {this.state.add && < AddHandler addValue={this.AddContact} onCloseAddForm = {this.closeModalForAddBtn} /> }
               {this.state.edit && < EditContact ContactEdit={this.editContactHandler} contactDetails={this.state.editContact} onClose = {this.closeModal} />}

               {
                    this.state.NewData.filter((val) =>{
                        if(!(val.first_name.toLowerCase().includes(this.state.SearchInput.toLowerCase())))
                        {
                           { <h3></h3>}
                        }
                    }
                    )
                    .map(contact => (
                        <div key={contact.id}>
                            <ul>
                                <img src={contact.avatar_url} />
                                <li>{contact.first_name + ' ' + contact.last_name}</li>
                                <li>{contact.email}</li>
                                <li>{contact.phone}</li>
                            </ul>
                            <div>
                                <button onClick={() => this.deleteHandler(contact.id)} className="btn btn-danger m-2">Delete</button>
                                <button onClick = {() => this.editFormWhenClick(contact)} type="button" className="btn btn-primary" data-toggle="modal" data-target="#editButton" >Edit</button>
                            </div>
                        </div>
                    ))
               }
           </div>
        );
    }
}
 
export default ShowData;